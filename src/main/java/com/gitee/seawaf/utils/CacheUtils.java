package com.gitee.seawaf.utils;

import java.io.InputStream;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.TransactionController;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.search.Attribute;
import net.sf.ehcache.search.Direction;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.Result;
import net.sf.ehcache.search.Results;

/**
 * 缓存工具类
 *  add the following code to web.xml
 *   <listener>
 *       <listener-class>net.sf.ehcache.constructs.web.ShutdownListener</listener-class>
 *   </listener>
 * @author haison
 *
 */
public class CacheUtils {
	
	private CacheManager cacheManager;
	private static CacheUtils cacheUtils;
	
	public CacheUtils(){
		InputStream stream = this.getClass().getResourceAsStream("/cache.xml");
		this.cacheManager = CacheManager.create(stream);
	}
	
	public static CacheUtils getInstance(){
		if(cacheUtils!=null){
			return cacheUtils;
		}else{
			cacheUtils = new CacheUtils();
			return cacheUtils;
		}
	}
	
	/**
	 * 增加键值对
	 * @param cacheName
	 * @param key
	 * @param value
	 */
	public void put(String cacheName,String key,Object value){
		Cache cache = this.cacheManager.getCache(cacheName);  
		if(cache!=null){
			Element element = new Element(key, value);  
			cache.put(element);  
		}
	}
	
	/**
	 * 获取cache中的键值
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public Object get(String cacheName,String key){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			Element element = cache.get(key);
			if(element!=null){
				return element.getObjectValue();
			}
		}
		return null;
	}
	
	/**
	 * 获取int值
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public int getIntValue(String cacheName,String key){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			return cache.get(key)==null?-1:Integer.parseInt(cache.get(key).getObjectValue().toString());
		}
		return -1;
	}
	
	/**
	 * 获取string值
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public String getStringValue(String cacheName,String key){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			return cache.get(key)==null?null:cache.get(key).getObjectValue().toString();
		}
		return null;
	}
	
	/**
	 * 获取cache
	 * @param name
	 * @return
	 */
	public Cache getCache(String name){
		return this.cacheManager.getCache(name);
	}
	
	/**
	 * 返回cache中的键列表
	 * @param cacheName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> keys(String cacheName){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			return (List<String>)cache.getKeys();
		}
		return null;
	}
	
	/**
	 * 根据值对键进行排序
	 * @param cacheName
	 * @param keyPattern
	 * @return
	 */
	public List<Result> keysOrderByIntValue(String cacheName,String keyPattern){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			Attribute<String> key = cache.getSearchAttribute("key");
			Attribute<Integer> value = cache.getSearchAttribute("value");
			Query query = cache.createQuery();
			query.includeAttribute(key).includeAttribute(value).includeKeys().includeValues();
			query.addCriteria(key.ilike(keyPattern)).addOrderBy(value, Direction.DESCENDING);
			Results results = query.execute();
			if(results!=null){
				return results.all();
			}
		}
		return null;
	}
	
	/**
	 * 根据查询条件返回cache中的键
	 * @param cacheName
	 * @param keyPattern
	 * @return
	 */
	public List<Result> keys(String cacheName,String keyPattern){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			Attribute<String> key = cache.getSearchAttribute("key");
			Query query = cache.createQuery();
			query.includeKeys().includeValues();
			query.addCriteria(key.ilike(keyPattern));
			Results results = query.execute();
			if(results!=null){
				return results.all();
			}
			
		}
		return null;
	}
	
	/**
	 * 计算cache中的键数量
	 * @param cacheName
	 * @return
	 */
	public int count(String cacheName){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			return cache.getSize();
		}
		return 0;
	}
	
	/**
	 * 删除键
	 * @param cacheName
	 * @param key
	 */
	public void remove(String cacheName,String key){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			cache.remove(key);
		}
	}
	
	/**
	 * 清空缓存
	 * @param cacheName
	 */
	public void clear(String cacheName){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			cache.removeAll();
		}
	}
	
	/**
	 * 保存缓存
	 * @param cacheName
	 */
	public void save(String cacheName){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			cache.flush();
		}
	}
	
	/**
	 * 关闭缓存
	 */
	public void shutdown(){
		this.cacheManager.shutdown();
	}
	
	/**
	 * 键值增加1
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public int enc(String cacheName,String key){
		TransactionController tc = this.cacheManager.getTransactionController();
		Cache cache = this.cacheManager.getCache(cacheName);
		int v=0;
		if(cache!=null){
			Element element = cache.get(key);
			tc.begin();
			if(element!=null){
				v = Integer.parseInt(element.getObjectValue().toString());
			}
			cache.put(new Element(key,v+1));
			tc.commit();
		}
		return v+1;
	}
	
	/**
	 * 自动生成键ID
	 * @param cacheName
	 * @param key
	 */
	public void autoKeyId(String cacheName,String key){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			if(cache.get(key)==null){
				cache.put(new Element(key,cache.getSize()+10001));
			}
		}
	}
	
	/**
	 * 判断cache中是否存在对应的key
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public boolean exists(String cacheName,String key){
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			return cache.get(key)!=null;
		}
		return false;
	}
	
	/**
	 * 计数器减少1
	 * @param cacheName
	 * @param key
	 */
	public void dec(String cacheName,String key){
		TransactionController tc = this.cacheManager.getTransactionController();
		Cache cache = this.cacheManager.getCache(cacheName);
		if(cache!=null){
			Element element = cache.get(key);
			if(element!=null){
				tc.begin();
				int v = Integer.parseInt(element.getObjectValue().toString());
				cache.put(new Element(key,v-1));
				tc.commit();
			}
		}
	}
	
	/**
	 * 创建一个缓存库
	 * @param cacheName
	 * @param ttl
	 * @param maxElement
	 */
	public void create(String cacheName,int ttl,int maxElement){
		Cache cache = new Cache(new CacheConfiguration(cacheName, maxElement).timeToLiveSeconds(ttl).timeToIdleSeconds(ttl));
		this.cacheManager.addCache(cache);
	}
	
	/**
	 * 返回缓存库中的所有缓存
	 * @return
	 */
	public String[] cacheNames(){
		return this.cacheManager.getCacheNames();
	}
}
